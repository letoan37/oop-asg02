package  bomberman;
/**
 * Class BombermanGame
 * @author Duog Hai Minh, Le Khanh Toan
 * @version 1.0
 * @since 2018-11-10
 */
import  bomberman.gui.Frame;

public class BombermanGame {
	
	public static void main(String[] args) {
		new Frame();
	}
}
