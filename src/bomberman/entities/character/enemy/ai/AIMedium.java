package  bomberman.entities.character.enemy.ai;
/**
 * Class AIMedium enemy tim duong di toi bomber
 * @author Duog Hai Minh, Le Khanh Toan
 * @version 1.0
 * @since 2018-11-10
 */
import bomberman.Board;
import  bomberman.entities.character.Bomber;
import  bomberman.entities.character.enemy.Enemy;
import javafx.util.Pair;
import java.util.LinkedList;


public class AIMedium extends AI {
	Bomber _bomber;
	Enemy _e;
	Board _board;
	public AIMedium(Board board,Bomber bomber, Enemy e) {
		_board = board;
		_bomber = bomber;
		_e = e;
	}
	
	@Override
	public int calculateDirection() {
		
		if(_bomber == null)
			return random.nextInt(4);
		
		int vertical = random.nextInt(2);
		
		if(vertical == 1) {
			int v = calculateRowDirection();
			if(v != -1)
				return v;
			else
				return calculateColDirection();
			
		} else {
			int h = calculateColDirection();
			
			if(h != -1)
				return h;
			else
				return calculateRowDirection();
		}
		
	}
	
	protected int calculateColDirection() {
		if(_bomber.getXTile() < _e.getXTile())
			return 3;
		else if(_bomber.getXTile() > _e.getXTile())
			return 1;
		
		return -1;
	}
	
	protected int calculateRowDirection() {
		if(_bomber.getYTile() < _e.getYTile())
			return 0;
		else if(_bomber.getYTile() > _e.getYTile())
			return 2;
		return -1;
	}
	
	
}
