package  bomberman.entities.tile;
/**
 * Class Wall
 * @author Duog Hai Minh, Le Khanh Toan
 * @version 1.0
 * @since 2018-11-10
 */

import  bomberman.graphics.Sprite;

public class Wall extends Tile {

	public Wall(int x, int y, Sprite sprite) {
		super(x, y, sprite);
	}

}
