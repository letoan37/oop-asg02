package  bomberman.graphics;
/**
 * Interface IRender
 * @author Duog Hai Minh, Le Khanh Toan
 * @version 1.0
 * @since 2018-11-10
 */
public interface IRender {

	void update();
	
	void render(Screen screen);
}
